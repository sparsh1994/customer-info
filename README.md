# README #

## Setup ##

> Clone the application from the https://bitbucket.org/sparsh1994/customer-info/.
> go to the application folder and install the dependencies via npm install

## Run ## 
(For checking the code in the local environment)
> 'node index' will run the application locally.

The api's have been build seperately both in MySql and MongoDB

> MySql code is available in develop branch
> MongoDB code is available in feat-mongodb branch

Note: On switching the branches make sure that you run npm install once.

#########    MySql   ########
> The domain for heroku server for mysql is : https://tricog-customer-mysql.herokuapp.com/

> get customer api : https://tricog-customer-mysql.herokuapp.com/api/customer/1 where 1 is the customer id. You need to have token and Content-type as headers and the token value can be taken from the post(create) customer response.

> create customer api : https://tricog-customer-mysql.herokuapp.com/api/customer
body is given in postman link shared.

##########  MongoDB   ##########
> The domain for heroku server for mysql is : https://tricog-customer.herokuapp.com/

> get customer api : https://tricog-customer.herokuapp.com/api/customer/id/1 where id is the customer field you want to query on and 1 is the value of that field(id in this case). 

In case of https://tricog-customer.herokuapp.com/api/customer/_id/5cea7735f427b36a4d227b23
search for a customer with _id = 5cea7735f427b36a4d227b23.

You need to have token and Content-type as headers and the token value can be taken from the post(create) customer response.

> create customer api : https://tricog-customer.herokuapp.com/api/customer
body is given in postman link shared.

You will get a token in the response body which is to be used a header token in get customer api.

## Note ##
> Images are available on google drive cloud and the link can be found in assets/images/links.txt

> Code has been deployed on heroku server, so for the first time the api response might take some time as heroku goes from idle to sleep state if not hit for some time, but once a api hit is recognized it switches back to active state.