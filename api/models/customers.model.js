export const createTableQuery = `CREATE TABLE IF NOT EXISTS customers (
    id int NOT NULL AUTO_INCREMENT,
    first_name varchar(255) NOT NULL,
    last_name varchar(255) NOT NULL,
    father_name varchar(255) NOT NULL,
    pan_number varchar(10) NOT NULL,
    dob varchar(8) NOT NULL,
    gender varchar(1) NOT NULL,
    email varchar(255) NOT NULL,
    address text NOT NULL,
    profile_image varchar(255) NOT NULL,
    PRIMARY KEY (id)
  )`;
  