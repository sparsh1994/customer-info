import SimpleCrypto from "simple-crypto-js";

import { Validators } from '../validators';
import { config } from '../../config/app-info';
import createTableQuery from '../models/customers.model';

const simpleCrypto = new SimpleCrypto(config.encryptionPassword);

export const createTable = (req, res) => {
    db.query(createTableQuery, (err, result) => {
        if (err) {
            res.end('Table already present');
        }
        res.end('customers table created');
    });
}

export const getCustomer = (req, res, id) => {
    if (authenticate(req)) {
        const sql = `SELECT * FROM customers WHERE id=${id}`;
        db.query(sql, (err, customer) => {
            if (!(customer && customer.length)) {
                res.writeHead(400, { 'Content-Type': 'text/html' });
                res.end('Customer not found.');
            }
            else if (customer) {
                res.writeHead(200, { 'Content-Type': 'application/json' });
                res.end(JSON.stringify(customer));
            } else if (err) {
                console.error(err.stack);
            }
        });
    } else {
        res.writeHead(409, { 'Content-Type': 'text/html' });
        res.end('Not authenticated');
    }
};

export const createCustomer = (req, res) => {
    let body = [];
    req.on('error', (err) => {
        console.error(err);
    }).on('data', (chunk) => {
        body.push(chunk);
    }).on('end', () => {
        body = Buffer.concat(body).toString();
        const reqSchemaErrors = validateRequest(JSON.parse(body));
        if (reqSchemaErrors.length) {
            res.writeHead(400, { 'Content-Type': 'application/json' });
            res.end(JSON.stringify(reqSchemaErrors));
        } else {
            body = JSON.parse(body);
            const sql = 'INSERT INTO customers SET ?';
            db.query(sql, body, (err, result) => {
                if (err) {
                    res.writeHead(400, { 'Content-Type': 'application/json' });
                    res.end(JSON.stringify(err));
                }
                else {
                    const responseObj = {
                        success: true,
                        message: "customer info saved",
                        user: result,
                        token: encryptResponse(config.tokenString)
                    };
                    res.writeHead(200, { 'Content-Type': 'application/json' });
                    res.end(JSON.stringify(responseObj));
                }
            });
        }
    });
}

function encryptResponse(token) {
    return simpleCrypto.encrypt(config.tokenString);
}

function authenticate(req) {
    return (req.headers && req.headers.token) ? simpleCrypto.decrypt(req.headers.token) == config.tokenString : false;
}

function validateRequest(reqBody) {
    let reqSchemaErrors = [];
    if (!(reqBody.first_name.match(Validators.NameValidator) && reqBody.last_name.match(Validators.NameValidator))) {
        reqSchemaErrors.push('Name should start with Capital letter and should be less than 20 characters');
    }
    if (!reqBody.father_name.match(Validators.FullNameValidation)) {
        reqSchemaErrors.push('Name should start with Capital letter and should be less than 20 characters');
    }
    if (!Validators.PanNumberValidaton.test(reqBody.pan_number)) {
        reqSchemaErrors.push('Enter correct pan number');
    }
    if (!reqBody.dob.match(Validators.DOB)) {
        reqSchemaErrors.push('Please enter date in yy/mm/dd format');
    }
    if (!((reqBody.first_name && reqBody.last_name) && (reqBody.father_name && reqBody.pan_number) &&
        (reqBody.dob && reqBody.gender) && (reqBody.email && reqBody.address) && reqBody.profile_image)) {
        reqSchemaErrors.push('All fields are mandatory');
    }
    return reqSchemaErrors;
}
