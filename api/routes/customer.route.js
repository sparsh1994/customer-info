import http from 'http';

import { getCustomer, createCustomer, createTable } from '../controllers/customer.controller.js';

export const customerRoute = http.createServer((req, res) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    const { url } = req;
    const id = url.split('/').pop();

    if (db) {
        // Empty route
        if (url == '/') {
            res.end('App works!');
        } else if (url == '/api/create-table' && req.method === 'GET') {
            createTable(req, res);
        } else if (url == `/api/customer/${id}` && req.method === 'GET') {
            getCustomer(req, res, id);
        } else if (url == '/api/customer' && req.method === 'POST') {
            createCustomer(req, res);
        } else {
            res.end('Invalid route');
        }
    }

});