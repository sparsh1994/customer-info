import mysql from 'mysql';

import { config } from './config/app-info';
import { customerRoute } from './api/routes/customer.route';

// Create connection
const db = mysql.createConnection({
    host: config.dbhost,
    port: config.dbport,
    user: config.dbuser,
    password: config.dbpassword,
    database: config.database
});

// connect
db.connect((err) => {
    if (err) {
        console.log('err', err.stack);
    }
    console.log('mysql connected');
});

global.db = db;
const server = customerRoute;

server.listen(config.port, () => {
    console.log('Server started on port ' + config.port);
});
